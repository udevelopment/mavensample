package com.demo;

import java.util.UUID;


public class Sample {
//	  public static void main(String[] args) {
//		       String[] words = {"Hello", "World", "using", "the", "Guava", "library"};
//		  
//		          String joined = Joiner.on(" ").join(words);
//		  
//		          System.out.println(joined);
//		  		      }
	 public static void main( String[] args )
	    {
	        Sample obj = new Sample();
	        System.out.println("Unique ID : " + obj.generateUniqueKey());
	    }
	    
	    public String generateUniqueKey(){
	    	
	    	String id = UUID.randomUUID().toString();
	    	return id;
	    	
	    }
}
